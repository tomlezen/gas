# -*- coding: utf-8 -*-
import os
import time
import xml.dom.minidom

import scrapy
from scrapy import Request, Selector

from Gas.items import GasItem


class GasSpider(scrapy.Spider):
    name = 'gas'
    allowed_domains = ['www.poi58.com']
    start_urls = ['http://www.poi58.com/search/s/?homecity_name=杭州&wd=中石化加油站']
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
    }

    def start_requests(self):
        # 先解析城市列表
        dom = xml.dom.minidom.parse(os.path.abspath('Gas/city.xml'))
        # 得到文档元素对象
        root = dom.documentElement
        # 获取省份
        provinces = root.getElementsByTagName('province')
        for prince in provinces:
            # 获取城市
            cities = prince.getElementsByTagName('city')
            for city in cities:
                city_name = city.getAttribute('name')
                yield Request('http://www.poi58.com/search/s/?homecity_name=' + city_name + '&wd=中石化加油站',
                              headers=self.headers)

    def parse(self, response):
        body = response.body.decode('utf-8').replace('OL', 'ol').replace('LI', 'li').replace('H2', 'h2')
        body_selector = Selector(text=body)
        out_tables = body_selector.xpath('//body/table/tr/td')
        for table in out_tables:
            items = table.xpath('.//table[@id="1"]/tr/td/ol/li')
            for item in items:
                yield self.parse_item(item)

        # 判断是否还有下一页
        next_page = body_selector.xpath('//div[@class="pg"]/a[last()]')
        next_page_text = next_page.xpath('text()').extract()
        if len(next_page_text) > 0 and next_page_text[0] == '下一页':
            next_url = next_page.xpath('@href').extract()[0]
            yield Request("http://www.poi58.com/search/s" + next_url, headers=self.headers, callback=self.parse)

    def parse_item(self, item):
        """
        解析每一个item
        :param item:
        :return:
        """
        gas_item = GasItem()
        gas_item['gid'] = time.time()
        try:
            gas_name_xpath = item.xpath('./h2/font')
            gas_sub_name = gas_name_xpath.xpath('text()').extract()
            # 这里有可能没有子标题
            if len(gas_sub_name) > 0:
                gas_sub_name = gas_sub_name[0].strip().replace('\n', '')
            else:
                gas_sub_name = ""
            gas_name_xpaths = gas_name_xpath.xpath('./font')
            gas_name = ''
            for gas_name_xpath in gas_name_xpaths:
                # 这里有时候会为空
                name = gas_name_xpath.xpath('text()').extract()
                if len(name) > 0:
                    gas_name += name[0]
            gas_name += gas_sub_name
            gas_item['name'] = gas_name.strip().replace('\n', '')
            divs = item.xpath('./div')
            if divs:
                gas_address = divs[0].xpath('.//div/text()').extract()[0]
                gas_item['address'] = gas_address.strip().replace('\n', '').replace('.', '')
                gas_locations = divs[2].xpath('.//div')
                gas_tencent_gd_location = gas_locations[1].xpath('text()').extract()[0]
                location = gas_tencent_gd_location.split('：')[1].strip().replace('\n', '').split(',')
                gas_item['location_tencent_gd_latitude'] = location[0]
                gas_item['location_tencent_gd_longitude'] = location[1]
                gas_baidu_location = gas_locations[2].xpath('text()').extract()[0]
                location = gas_baidu_location.split('：')[1].strip().replace('\n', '').split(',')
                gas_item['location_baidu_latitude'] = location[0]
                gas_item['location_baidu_longitude'] = location[1]
        except Exception as e:
            self.logger.exception(str(e))
        finally:
            return gas_item
