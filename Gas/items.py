# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field


class GasItem(scrapy.Item):
    # id 时间参数
    gid = Field()
    # 城市
    city = Field()
    # 加油站名字
    name = Field()
    # 子名字
    sub_name = Field()
    # 加油站地址
    address = Field()
    # 加油站百度经纬度
    location_baidu_latitude = Field()
    location_baidu_longitude = Field()
    # 加油站腾讯和高德定位
    location_tencent_gd_latitude = Field()
    location_tencent_gd_longitude = Field()


class City(scrapy.Item):
    """
    城市
    """
    # 城市名
    name = Field()


class Province(scrapy.Item):
    """
    省份
    """
    # 城市列表
    cities = Field()
