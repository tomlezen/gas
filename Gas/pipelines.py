# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import logging

import pymongo as pymongo
import six

logger = logging.getLogger(__name__)


class MongoDBPipeline(object):
    def __init__(self, mongodb_server, mongodb_db, mongodb_collection, mongodb_uniq_key, mongodb_item_id_field):
        connection = pymongo.MongoClient(mongodb_server)
        self.mongodb_db = mongodb_db
        self.db = connection[mongodb_db]
        self.mongodb_collection = mongodb_collection
        self.collection = self.db[mongodb_collection]
        self.uniq_key = mongodb_uniq_key
        self.item_id = mongodb_item_id_field

        if isinstance(self.uniq_key, six.string_types) and self.uniq_key == "":
            self.uniq_key = None

        if self.uniq_key:
            self.collection.create_index(self.uniq_key, unique=True)

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        return cls(settings.get('MONGODB_SERVER', None),
                   settings.get('MONGODB_DB', None),
                   settings.get('MONGODB_COLLECTION', None),
                   settings.get('MONGODB_UNIQ_KEY', None),
                   settings.get('MONGODB_ITEM_ID_FIELD', None))

    def process_item(self, item, spider):
        if self.uniq_key is None:
            result = self.collection.insert_one(dict(item))
        else:
            result = self.collection.update_one({self.uniq_key: item[self.uniq_key]}, {'$set': dict(item)}, upsert=True)

        if self.item_id in item.fields and not item.get(self.item_id, None):
            item[self.item_id] = result

        logger.debug("Item %s wrote to MongoDB database %s/%s, spider: %s" % (
            getattr(result, 'inserted_id', getattr(result, 'upserted_id')),
            self.mongodb_db,
            self.mongodb_collection,
            spider.name))
        return item
